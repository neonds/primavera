package net.bac.primavera.webapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

 @Autowired
 @Qualifier("UserDetailsServiceImpl")
 private UserDetailsService userDetailsService;

 /**
  * Configuración del servicio que resuelve los usuarios de la aplicación
  *
  * @param auth Inyectado por Spring
  * @throws Exception En caso de haber un error a la hora de obtener algun usuario
  */
 @Override
 public void configure(AuthenticationManagerBuilder auth) throws Exception {
   auth.userDetailsService(userDetailsService);
 }

 /**
  * Configuración de seguridad de la aplicación web
  *
  * @param http Recibe un HttpSecuriy inyectado por Spring
  * @throws Exception Lanza una excepción en caso de romper alguna de las relgas o al haber una mala configuración.
  */
 @Override
 protected void configure(HttpSecurity http) throws Exception {
   http
           .authorizeRequests()
           .antMatchers("/webjars/**").permitAll()
           .antMatchers("/resources/**").permitAll()
           .antMatchers("/users/logout").permitAll()
           .anyRequest().authenticated()
           .and()
           .formLogin()
           .loginPage("/users/auth")
           .permitAll();
 }

}
