/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package net.bac.primavera.webapp.controller.home;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.bac.primavera.webapp.controller.home.model.Sample;


/**
 * @author gdiazs
 *
 * @since 14 jul. 2016
 */
@Controller
@RequestMapping("/home")
public class HomeController {

	@RequestMapping(method=RequestMethod.GET)
	public String index(Model model){
		model.addAttribute("sample", new Sample());
		return "home/index";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String saveSample(@Valid Sample sample, Errors errors, Model model){
		model.addAttribute("sample", sample);
		System.out.println(sample);
		if (errors.hasErrors()){
			System.out.println("Errors");	
		}
		
		return  "home/index";
	}
}
