package net.bac.primavera.webapp.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Guillermo B Díaz Solís
 * @since 12 de jul. de 2016
 */
public class WebSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}