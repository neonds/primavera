/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package thymeleaf.spring.support;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author gdiazs
 *
 * @since 14 jul. 2016
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Layout {
	String value();
}
