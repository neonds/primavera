package net.bac.primavera.repository.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.bac.primavera.repository.model.entity.Authority;

/**
 * @author Guillermo
 */
@Repository
public interface AuthorityDao extends CrudRepository<Authority, Long>  {

}
