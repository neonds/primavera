package net.bac.primavera.repository.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.bac.primavera.repository.model.entity.User;

/**
 * @author Guillermo
 */
@Repository
public interface UserDao extends CrudRepository<User, Long> {

	@Query("select usr from User usr where username = :username")
	User findByUsername(@Param("username") String userName);
}
