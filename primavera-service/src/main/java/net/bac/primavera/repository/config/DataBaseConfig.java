package net.bac.primavera.repository.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Guillermo
 */
/**
 * @author Guillermo B Díaz Solís
 * @since 29 de may. de 2016
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("net.bac.primavera.repository.dao")
@PropertySource(value = "classpath:database.properties")
public class DataBaseConfig {

	@Autowired
	private Environment env;

	/**
	 * Permite inyectar en variables Stringlas anotaciones definidas
	 * en @PropertySource
	 * 
	 * @return Una instancia de PropertySourcesPlaceholderConfigurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();

	}

	/**
	 * Es una factoria para el repositorio JPA . Realiza un scan en el paquete
	 * de entidades com.guillermods.shopy.repository.entity
	 * 
	 * @return Un contenedor de entidades
	 * @throws SQLException
	 *             Lanza excepción en caso de un error en el mapeo de las tablas
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan("net.bac.primavera.repository.model.entity");
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());
		return em;
	}

	/**
	 * Todas las configuraciones adicionales de JPA se hacen a través de las
	 * propiedaes definidas en este método
	 * 
	 * @return Una objecto de Properties con las configuraciones de JPA
	 */
	public Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		return properties;
	}

	/**
	 * Realiza scan del contexto del servidor tomcat y obtiene la conexión
	 * mediante JNDI
	 * 
	 * @return Una instancia de DataSource con la conexión de base de datos
	 * @throws SQLException
	 *             En caso de no haber una configuración correcta o de no
	 *             encontrar la base de datos.
	 */
	@Bean
	public DataSource dataSource() throws SQLException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("db.dev.driverClass"));
		dataSource.setUrl(env.getProperty("db.dev.urljdbc"));
		dataSource.setUsername(env.getProperty("db.dev.user"));
		dataSource.setPassword(env.getProperty("db.dev.password"));
		return dataSource;
	}

	/**
	 * Configuración de transacciones de Spring Framework para el repositorio
	 * JPA
	 * 
	 * @return Devuelve un manejador de Transacciones JPA
	 * @throws SQLException
	 *             Error en caso de no poder instanciar el manager
	 */
	@Bean
	public JpaTransactionManager transactionManager() throws SQLException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
}
