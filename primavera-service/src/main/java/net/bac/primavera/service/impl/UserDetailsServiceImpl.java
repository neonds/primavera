package net.bac.primavera.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.bac.primavera.repository.dao.UserDao;
import net.bac.primavera.service.model.dto.UserDto;
import net.bac.primavera.service.model.mapper.UserMapper;

/**
 * Implementación del servicio de UserDetailService, utiliza el repositorio de usuarios para obtener un usuario
 * en cuestión
 *
 * @author Guillermo B Díaz Solís
 * @since 12 de jul. de 2016
 */
@Service("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserDao userDao;

  @Autowired
  private UserMapper userMapper;

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetailsService#
   * loadUserByUsername(java.lang.String)
   */
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDto userDto = userMapper.mapToDto(userDao.findByUsername(username));
    return userDto;
  }

}