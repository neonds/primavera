package net.bac.primavera.service.model.mapper;

import org.springframework.stereotype.Component;

import net.bac.primavera.repository.model.entity.Authority;
import net.bac.primavera.service.ServiceMapper;
import net.bac.primavera.service.model.dto.AuthorityDto;

/**
 * Implementación de ServiceMapper. Permite convertir un AuthorityDto a Authority y viceversa
 * @author Guillermo B Díaz Solís
 * @since 16 de jun. de 2016
 */
@Component
public class AuthorityMapper implements ServiceMapper<Authority, AuthorityDto> {

  /* (non-Javadoc)
   * @see com.guillermods.shopy.service.ServiceMapper#map(java.lang.Object, java.lang.Object)
   */
  @Override
  public AuthorityDto map(Authority source, AuthorityDto target) {
    target.setAuthority(source.getAuthority());
    target.setCreatedAt(source.getCreatedAt());
    target.setEnabled(source.isEnabled());
    target.setId(source.getId());
    target.setUpdatedAt(source.getUpdatedAt());
    return target;
  }

  /* (non-Javadoc)
   * @see com.guillermods.shopy.service.ServiceMapper#mapToDto(java.lang.Object)
   */
  @Override
  public AuthorityDto mapToDto(Authority source) {
    AuthorityDto target = new AuthorityDto();
    target.setAuthority(source.getAuthority());
    target.setCreatedAt(source.getCreatedAt());
    target.setEnabled(source.isEnabled());
    target.setId(source.getId());
    target.setUpdatedAt(source.getUpdatedAt());
    return target;
  }

  /* (non-Javadoc)
   * @see com.guillermods.shopy.service.ServiceMapper#mapToEntity(java.lang.Object)
   */
  @Override
  public Authority mapToEntity(AuthorityDto source) {
    Authority target = new Authority();
    target.setAuthority(source.getAuthority());
    target.setCreatedAt(source.getCreatedAt());
    target.setEnabled(source.isEnabled());
    target.setId(source.getId());
    target.setUpdatedAt(source.getUpdatedAt());
    return target;
  }

}