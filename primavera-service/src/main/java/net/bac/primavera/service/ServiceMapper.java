package net.bac.primavera.service;

/**
 * @author Guillermo B Díaz Solís
 * @since 16 de jun. de 2016
 */
public interface ServiceMapper<S, T> {

  /**
   * Realiza un mapeo de objeto origen al objeto destino
   *
   * @param source Objeto origen
   * @param target Objeto destino
   * @return El objeto destino
   */
  T map(S source, T target);

  /**
   * Realiza un mapeo a DTO
   *
   * @param source Objeto entidad
   * @return Objeto DTO
   */
  T mapToDto(S source);

  /**
   * Realiza un mapeo a Entidad
   *
   * @param source Objeto DTO
   * @return Objeto Entidad
   */
  S mapToEntity(T source);

}