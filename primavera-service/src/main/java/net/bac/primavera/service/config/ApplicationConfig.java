package net.bac.primavera.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import net.bac.primavera.repository.config.DataBaseConfig;

/**
 * @author Guillermo
 */
@Import(value = {DataBaseConfig.class})
@ComponentScan("net.bac.primavera.service")
public class ApplicationConfig {

}
