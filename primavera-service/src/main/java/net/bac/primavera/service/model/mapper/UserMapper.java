package net.bac.primavera.service.model.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.bac.primavera.repository.model.entity.Authority;
import net.bac.primavera.repository.model.entity.User;
import net.bac.primavera.service.ServiceMapper;
import net.bac.primavera.service.model.dto.AuthorityDto;
import net.bac.primavera.service.model.dto.UserDto;

/**
 * Implementación de ServiceMapper, permite convertir un UserDto a User y
 * viceversa
 *
 * @author Guillermo B Díaz Solís
 * @since 16 de jun. de 2016
 */
@Component
public class UserMapper implements ServiceMapper<User, UserDto> {

	private ServiceMapper<Authority, AuthorityDto> authorityMapper;

	/**
	 * @param authorityMapper
	 *            Una instancia de ServiceMapper
	 */
	@Autowired
	public UserMapper(ServiceMapper<Authority, AuthorityDto> authorityMapper) {
		this.authorityMapper = authorityMapper;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.guillermods.shopy.service.ServiceMapper#map(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public UserDto map(User source, UserDto target) {
		target.setAccountNonExpired(source.isAccountNonExpired());
		target.setAccountNonLocked(source.isAccountNonLocked());

		List<AuthorityDto> authoritiesDto = new ArrayList<AuthorityDto>();
		List<Authority> authorities = source.getAuthorities();

		for (Authority authority : authorities) {
			authoritiesDto.add(this.authorityMapper.mapToDto(authority));
		}

		target.setCreatedAt(source.getCreatedAt());
		target.setCredentialsNonExpired(source.isCredentialsNonExpired());
		target.setDateBirth(source.getDateBirth());
		target.setEmail(source.getEmail());
		target.setEnabled(source.isEnabled());
		target.setFathersName(source.getFathersName());
		target.setId(source.getId());
		target.setLastPasswordResetDate(source.getLastPasswordResetDate());
		target.setMothersName(source.getMothersName());
		target.setName(source.getName());
		target.setPassword(source.getPassword());
		target.setUpdatedAt(source.getUpdatedAt());
		target.setUsername(source.getUsername());

		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.shopy.service.ServiceMapper#mapToDto(java.lang.Object)
	 */
	@Override
	public UserDto mapToDto(User source) {

		UserDto target = new UserDto();
		target.setAccountNonExpired(source.isAccountNonExpired());
		target.setAccountNonLocked(source.isAccountNonLocked());

		List<AuthorityDto> authoritiesDto = new ArrayList<AuthorityDto>();
		List<Authority> authorities = source.getAuthorities();

		for (Authority authority : authorities) {
			authoritiesDto.add(this.authorityMapper.mapToDto(authority));
		}

		target.setCreatedAt(source.getCreatedAt());
		target.setCredentialsNonExpired(source.isCredentialsNonExpired());
		target.setAuthorities(authoritiesDto);
		target.setDateBirth(source.getDateBirth());
		target.setEmail(source.getEmail());
		target.setEnabled(source.isEnabled());
		target.setFathersName(source.getFathersName());
		target.setId(source.getId());
		target.setLastPasswordResetDate(source.getLastPasswordResetDate());
		target.setMothersName(source.getMothersName());
		target.setName(source.getName());
		target.setPassword(source.getPassword());
		target.setUpdatedAt(source.getUpdatedAt());
		target.setUsername(source.getUsername());

		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.shopy.service.ServiceMapper#mapToEntity(java.lang.Object)
	 */
	@Override
	public User mapToEntity(UserDto source) {
		User target = new User();
		target.setAccountNonExpired(source.isAccountNonExpired());
		target.setAccountNonLocked(source.isAccountNonLocked());

		List<AuthorityDto> authoritiesDto = source.getAuthorities();
		List<Authority> authorities = new ArrayList<Authority>();

		for (AuthorityDto authorityDto : authoritiesDto) {
			authorities.add(this.authorityMapper.mapToEntity(authorityDto));
		}

		target.setCreatedAt(source.getCreatedAt());
		target.setCredentialsNonExpired(source.isCredentialsNonExpired());
		target.setAuthorities(authorities);
		target.setDateBirth(source.getDateBirth());
		target.setEmail(source.getEmail());
		target.setEnabled(source.isEnabled());
		target.setFathersName(source.getFathersName());
		target.setId(source.getId());
		target.setLastPasswordResetDate(source.getLastPasswordResetDate());
		target.setMothersName(source.getMothersName());
		target.setName(source.getName());
		target.setPassword(source.getPassword());
		target.setUpdatedAt(source.getUpdatedAt());
		target.setUsername(source.getUsername());

		return target;
	}

}