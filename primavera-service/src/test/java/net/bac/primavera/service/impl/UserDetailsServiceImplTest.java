package net.bac.primavera.service.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

import net.bac.primavera.service.config.AbstractApplicationConfigTest;

public class UserDetailsServiceImplTest extends AbstractApplicationConfigTest {
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	private UserDetailsService userService;

	/**
	 * Test method for
	 * {@link com.guillermods.shopy.service.impl.UserDetailsServiceImpl#loadUserByUsername(java.lang.String)}.
	 */
	@Test
	@Transactional
	public void testLoadUserByUsername() {
		UserDetails userDetails = userService.loadUserByUsername("neonds");
		assertNotNull(userDetails);
	}

}
