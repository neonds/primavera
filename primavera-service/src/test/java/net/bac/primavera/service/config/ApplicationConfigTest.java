package net.bac.primavera.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.bac.primavera.repository.config.DataBaseConfigTest;

@Configuration
@ComponentScan("net.bac.primavera.service")
@Import(DataBaseConfigTest.class)
public class ApplicationConfigTest {
	

}
