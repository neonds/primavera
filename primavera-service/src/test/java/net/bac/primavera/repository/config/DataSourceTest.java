package net.bac.primavera.repository.config;

import javax.sql.DataSource;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Prueba para validar la inyección de la base de datos en memoria. Asegura que
 * la migración de los SQL sea efectiva
 */
public class DataSourceTest extends AbstractDatabaseConfigurationTest {

	@Autowired
	private DataSource datasource;

	/**
	 * Prueba que el datasource esté instanciado
	 */
	@Test
	public void databaseMigrationTest() {
		assertNotNull(datasource);
	}
}
