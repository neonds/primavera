package net.bac.primavera.repository.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@EnableJpaRepositories("net.bac.primavera.repository.dao")
public class DataBaseConfigTest {

	/**
	 * Usuario por defecto de H2 DB
	 */
	private static final String USER_DB = "sa";

	/***
	 * JDBC URL de base de datos en memoria
	 */
	private static final String JDBC_URL = "jdbc:h2:mem:migrationtestdb;DB_CLOSE_DELAY=5;TRACE_LEVEL_SYSTEM_OUT=1";

	/**
	 * Crea un datasource después de ejecutar flyway
	 * 
	 * @return DataSource Una instancia de H2 DB en memoria
	 */
	@Bean
	public DataSource dataSource() {
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUsername(USER_DB);
		dataSource.setPassword("");
		dataSource.setUrl(JDBC_URL);

		// Flyway toma todos los archivos de base de datos versionados y los
		// ejecuta uno a uno.
		// Esto asegura que los scripts son consistentes
		Flyway flyway = new Flyway();
		flyway.setLocations("classpath:db/migration");
		flyway.setDataSource(dataSource);
		flyway.migrate();
		return flyway.getDataSource();
	}

	/**
	 * Es una factoria para el repositorio JPA . Realiza un scan en el paquete
	 * de entidades com.guillermods.shopy.repository.entity
	 *
	 * @return Un contenedor de entidades
	 * @throws SQLException
	 *             Lanza excepción en caso de un error en el mapeo de las tablas
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan("net.bac.primavera.repository.model.entity");

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}

	/**
	 * Todas las configuraciones adicionales de JPA se hacen a través de las
	 * propiedaes definidas en este método
	 *
	 * @return Una objecto de Properties con las configuraciones de JPA
	 */
	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		return properties;
	}

	/**
	 * Configuración de transacciones de Spring Framework para el repositorio
	 * JPA
	 *
	 * @return Devuelve un manejador de Transacciones JPA
	 * @throws SQLException
	 *             Error en caso de no poder instanciar el manager
	 */
	@Bean
	public JpaTransactionManager transactionManager() throws SQLException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
}
