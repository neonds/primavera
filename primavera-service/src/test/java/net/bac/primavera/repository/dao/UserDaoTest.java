package net.bac.primavera.repository.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import static org.hamcrest.CoreMatchers.is;

import net.bac.primavera.repository.config.AbstractDatabaseConfigurationTest;
import net.bac.primavera.repository.model.entity.User;

public class UserDaoTest extends AbstractDatabaseConfigurationTest {

	@Autowired
	private UserDao userDao;

	@Test
	public void testUserDaoInjection() {
		assertNotNull(userDao);
	}

	@Test
	@Transactional
	public void testFindOne() {
		String expected = "neonds";
		User user = userDao.findOne(1L);
		Assert.assertThat(expected, is(user.getUsername()));

	}

}
