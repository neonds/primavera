package net.bac.primavera.repository.config;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Esta clase es utilizada para hacer Integration Test con la base de datos
 * local
 * 
 * @author Guillermo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataBaseConfig.class, loader = AnnotationConfigContextLoader.class)
public class DataBaseConfigIntegrationLocalTest {

	@Autowired
	private DataSource dataSource;
	
	@Test
	public void IntegrationTestDataSourceLocalIsNotNull(){
		assertNotNull(dataSource);
		
	}
}
